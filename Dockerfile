# Use an OpenJDK Runtime as a parent image
FROM openjdk:8-jre-alpine
# Define environment variables
ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \JAVA_OPTS=""
# Set the working directory to /app
WORKDIR /app
# Copy the executable into the container at /app
ADD target/*.jar Enseignants-0.0.1-SNAPSHOT.jar
# Make port 8080 available to the world outside this container
EXPOSE 8080
# Make port 8080 available to the world outside this container
EXPOSE 8090
# Run app.jar when the container launches
CMD ["java", "-jar", "/app/Enseignants-0.0.1-SNAPSHOT.jar"] 
