package com.sesame.Enseignants.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sesame.Enseignants.data.CoursRepository;
import com.sesame.Enseignants.model.Classe;
import com.sesame.Enseignants.model.Cours;
import com.sesame.Enseignants.model.Enseignant;

@RestController
@RequestMapping("/rest/cours")
public class CoursRestController {
	@Autowired(required = false)
	private CoursRepository coursRepositry;

	@PostMapping(path = "/insert", consumes="application/json")
	public void insert(@RequestBody Cours cours) {
		RestTemplate restTemplate = new RestTemplate();
		if (cours.getEns() != null && cours.getClasse() != null) {
			Enseignant ens = restTemplate.getForObject("http://localhost:8080/rest/enseignants/" + cours.getEns().getId(),
					Enseignant.class);
			Classe classe = restTemplate
					.getForObject("http://localhost:8080/rest/classes/" + cours.getClasse().getNomClasse(), Classe.class);
			if (ens != null && classe != null) {
				coursRepositry.save(cours);
			}
		}

	}

	@GetMapping(path = "/all", produces = "application/json")
	public @ResponseBody List<Cours> listAllEns() {
		return coursRepositry.findAll();
	}

	@GetMapping(path = "/{id}", produces = "application/json")
	public @ResponseBody Cours getEnsById(@PathVariable("id") String id) {
		return coursRepositry.findById(id).orElseGet(() -> null);
	}

	@GetMapping(path = "/emploi/{id}", produces = "application/json")
	public @ResponseBody List<Cours> getEmploiEns(@PathVariable("id") Long id) {
		RestTemplate restTemplate = new RestTemplate();
		Enseignant ens = restTemplate.getForObject("http://localhost:8080/rest/enseignants/" + id, Enseignant.class);
		if (ens != null) {
			return coursRepositry.findByEns(ens);
		}
		return null;

	}

}
