package com.sesame.Enseignants.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sesame.Enseignants.data.ClasseRepository;
import com.sesame.Enseignants.model.Classe;

@RestController
@RequestMapping("/rest/classes")
public class ClasseRestController {
    @Autowired
    private ClasseRepository classeRepo;

    @GetMapping(path="/all", produces= "application/json")
    public @ResponseBody List<Classe> listAllEns() {
    	return classeRepo.findAll();
    }

    @GetMapping(path="/{id}", produces= "application/json")
    public @ResponseBody Classe getEnsById(@PathVariable("id") String id) {
    	return classeRepo.findById(id).orElseGet(() -> null);
    }
    
    @PostMapping(path="/insert", consumes="application/json")
    public void insert(@RequestBody Classe Classe) {
    	classeRepo.save(Classe);
    }
    
    @DeleteMapping(path="/remove/{id}")
    public void remove(@PathVariable("id") String id) {
    	classeRepo.deleteById(id);
    }
    
    @DeleteMapping(path="/removeAll")
    public void removeAll() {
    	classeRepo.deleteAll();
    }
    
}
