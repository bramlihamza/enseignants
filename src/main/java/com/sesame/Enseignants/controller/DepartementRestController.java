package com.sesame.Enseignants.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sesame.Enseignants.data.DepartementRepository;
import com.sesame.Enseignants.model.Departement;

@RestController
@RequestMapping("/rest/departements")
public class DepartementRestController {
    @Autowired
    private DepartementRepository depRepo;

    @GetMapping(path="/all", produces= "application/json")
    public @ResponseBody List<Departement> listAllEns() {
        return depRepo.findAll();
    }

    @GetMapping(path="/{id}", produces= "application/json")
    public @ResponseBody Departement getEnsById(@PathVariable("id") String id) {
        return depRepo.findById(id).orElseGet(() -> null);
    }
    
    @PostMapping(path="/insert", consumes="application/json")
    public void insert(@RequestBody Departement Departement) {
    	depRepo.save(Departement);
    }
    
    @DeleteMapping(path="/remove/{id}")
    public void remove(@PathVariable("id") String id) {
    	depRepo.deleteById(id);
    }
    
    @DeleteMapping(path="/removeAll")
    public void removeAll() {
    	depRepo.deleteAll();
    }
    
}
