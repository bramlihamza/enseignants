package com.sesame.Enseignants.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sesame.Enseignants.data.UtilisateurRepository;
import com.sesame.Enseignants.model.Utilisateur;



@RestController
@RequestMapping("/rest/admins")

public class UtilisateurRestController {
    @Autowired
    private UtilisateurRepository depRepo;

    @Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder ;
    
    @GetMapping(path="/all", produces= "application/json")
    public @ResponseBody List<Utilisateur> listAllEns() {
        return depRepo.findAll();
    }

    
    @PostMapping(path="/insert", consumes="application/json")
    public void insert(@RequestBody Utilisateur utilisateur) {
    	String hashPW=bCryptPasswordEncoder.encode(utilisateur.getPassword());
		utilisateur.setPassword(hashPW);
		
    	utilisateur.setRole("admin");
    	depRepo.save(utilisateur);
    }
    
    @PatchMapping("/update/{id}")
    public ResponseEntity<?> partialUpdate(
      @RequestBody Utilisateur partialUpdate, @PathVariable("id") Long id) {
        Optional<Utilisateur> opt = depRepo.findById(id);
        if (opt.isPresent()) {
			Utilisateur user = opt.get();
			if (partialUpdate.getEmail() != null)
				user.setEmail(partialUpdate.getEmail());
			if (partialUpdate.getNom() != null)
				user.setNom(partialUpdate.getNom());
			if (partialUpdate.getPrenom() != null)
				user.setPrenom(partialUpdate.getPrenom());
			if (partialUpdate.getRole() != null)
				user.setRole(partialUpdate.getRole());
			if (partialUpdate.getType() != null)
				user.setType(partialUpdate.getType());
        	depRepo.save(user);
        }
    	
        return ResponseEntity.ok("resource address updated");
    }
  
    
}
