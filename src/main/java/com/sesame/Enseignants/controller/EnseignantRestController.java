/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sesame.Enseignants.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sesame.Enseignants.data.EnseignantRepository;
import com.sesame.Enseignants.model.Departement;
import com.sesame.Enseignants.model.Enseignant;

@RestController
@RequestMapping("/rest/enseignants")
public class EnseignantRestController {
    @Autowired
    private EnseignantRepository ensRepo;
    
    @Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder ;

    @GetMapping(path="/all", produces= "application/json")
    public @ResponseBody List<Enseignant> listAllEns() {
        return ensRepo.findAll();
    }

    @GetMapping(path="/{id}", produces= "application/json")
    public @ResponseBody Enseignant getEnsById(@PathVariable("id") Long id) {
        return ensRepo.findById(id).orElseGet(() -> null);
    }
    
    @PostMapping(path="/insert")
    public void insert(@RequestBody Enseignant enseignant) {
    	String hashPW=bCryptPasswordEncoder.encode(enseignant.getPassword());
    	enseignant.setPassword(hashPW);
    	enseignant.setRole("enseignant");
    	ensRepo.save(enseignant);
    }
    
    @DeleteMapping(path="/remove/{id}", produces= "application/json")
    public void remove(@PathVariable("id") Long id) {
    	ensRepo.deleteById(id);
    }
    
    @DeleteMapping(path="/removeAll")
    public void removeAll() {
    	ensRepo.deleteAll();
    }
    
    @GetMapping(path="/departement/{id}", produces="application/json")
    public @ResponseBody List<Enseignant> getEnsByDep(@PathVariable("id") String id) {
    	return ensRepo.findByDepartement(new Departement(id));
    }
}
