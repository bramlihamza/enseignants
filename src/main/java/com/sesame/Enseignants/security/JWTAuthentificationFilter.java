package com.sesame.Enseignants.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sesame.Enseignants.model.Utilisateur;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthentificationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager ;
	
	
	public JWTAuthentificationFilter(AuthenticationManager authenticationManager) {
		super();
		this.authenticationManager = authenticationManager;
		}

	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		// TODO Auto-generated method stub
		
		Utilisateur utilisateur = null ;


		try {
		/*recuperation du json envoyer par l'utilisateur pour s'authentifier 
		 et le stocker ou caster dans un objet Utilisateur */
		utilisateur = new ObjectMapper().readValue(request.getInputStream(),Utilisateur.class ) ;

		} catch (Exception e) {
		throw new RuntimeException(e) ;
		}
		System.out.println("===>"+utilisateur.getEmail());
		System.out.println("===>"+utilisateur.getPassword());
		
		return authenticationManager.authenticate(
		new UsernamePasswordAuthenticationToken(utilisateur.getEmail(), utilisateur.getPassword()));

		
		
	}


	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		User springuser = (User) authResult.getPrincipal();
	    
        String jwt = Jwts.builder().setSubject(springuser.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SecurityConstants.SECRET)
                .claim("roles", springuser.getAuthorities())
                .compact();
        System.out.println(jwt);
        System.out.println(springuser.getAuthorities().toString());
        response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + jwt);
        
        response.getWriter().append("{"+'"'+"token" +'"'+':' + '"'+ 
        		   SecurityConstants.TOKEN_PREFIX + jwt +'"' +'}'  ) ;
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
	}
	
	
	

	
	
}
