package com.sesame.Enseignants.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sesame.Enseignants.data.UtilisateurRepository;
import com.sesame.Enseignants.model.Utilisateur;



@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	 
	@Autowired
	UtilisateurRepository utilisateurService ;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Utilisateur utilisateur = utilisateurService.findByEmail(username);
		
		if (utilisateur == null) throw new UsernameNotFoundException(username) ;

		Collection<GrantedAuthority>  authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(utilisateur.getRole())) ;
		 
		return new User(utilisateur.getEmail(),utilisateur.getPassword(),authorities);

	}

}
