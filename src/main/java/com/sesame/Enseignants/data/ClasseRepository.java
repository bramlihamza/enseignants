package com.sesame.Enseignants.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sesame.Enseignants.model.Classe;


public interface ClasseRepository extends JpaRepository<Classe, String> {

}
