package com.sesame.Enseignants.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sesame.Enseignants.model.Departement;

public interface DepartementRepository extends JpaRepository<Departement, String> {

}
