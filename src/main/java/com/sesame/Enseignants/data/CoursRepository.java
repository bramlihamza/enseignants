package com.sesame.Enseignants.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sesame.Enseignants.model.Cours;
import com.sesame.Enseignants.model.Enseignant;

public interface CoursRepository extends JpaRepository<Cours, String> {

	List<Cours> findByEns(Enseignant ens);
}
