package com.sesame.Enseignants.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Departement")
public class Departement implements Serializable {

	@Id
	private String nomDep;
	
	@OneToMany(mappedBy = "departement", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<Enseignant> enseignants;

	public String getNomDep() {
		return nomDep;
	}

	public void setNomDep(String nomDep) {
		this.nomDep = nomDep;
	}

	public List<Enseignant> getEnseignants() {
		return enseignants;
	}

	public void setEnseignants(List<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}

	public Departement(String nomDep) {
		super();
		this.nomDep = nomDep;
	}

	public Departement(String nomDep, List<Enseignant> enseignants) {
		super();
		this.nomDep = nomDep;
		this.enseignants = enseignants;
	}
	
	public Departement() {}
}
