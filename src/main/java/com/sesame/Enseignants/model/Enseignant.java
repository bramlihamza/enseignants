/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sesame.Enseignants.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;



@Entity
@Table(name = "Enseignant")
@PrimaryKeyJoinColumn(name ="id")
@Inheritance(strategy=InheritanceType.JOINED)  

public class Enseignant extends Utilisateur implements Serializable   {



	@ManyToOne(fetch = FetchType.LAZY)
    
    private Departement departement;
	
	@OneToMany(mappedBy = "ens")
    private Set<Cours> cours;

	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Enseignant(Departement departement, Set<Cours> cours) {
		super();
		
		this.departement = departement;
		this.cours = cours;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Set<Cours> getCours() {
		return cours;
	}

	public void setCours(Set<Cours> cours) {
		this.cours = cours;
	}

	@Override
	public String toString() {
		return "Enseignant [departement=" + departement + ", cours=" + cours + "]";
	}
	
	

}
