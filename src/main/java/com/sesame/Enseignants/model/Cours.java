package com.sesame.Enseignants.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class Cours implements Serializable {

	@EmbeddedId
	private CoursId id;
	@MapsId("ensKey")
	@ManyToOne
	private Enseignant ens;

	@ManyToOne
	@JoinColumn(name = "classe_id")
	private Classe classe;

	private LocalDateTime startTime;
	private LocalDateTime finishTime;
	private int salle;

	public Cours(CoursId id, Enseignant ens, Classe classe, LocalDateTime startTime, LocalDateTime finishTime,
			int salle) {
		super();
		this.id = id;
		this.ens = ens;
		this.classe = classe;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.salle = salle;
	}

	public Cours() {
	}

	public String getNomMatiere() {
		return id.getNomMatiere();
	}

	public Enseignant getEns() {
		return ens;
	}

	public void setEns(Enseignant ens) {
		this.ens = ens;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(LocalDateTime finishTime) {
		this.finishTime = finishTime;
	}

	public int getSalle() {
		return salle;
	}

	public void setSalle(int salle) {
		this.salle = salle;
	}

}
