package com.sesame.Enseignants.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CoursId implements Serializable {
	private String nomMatiere;
	private Long ensKey;

	public String getNomMatiere() {
		return nomMatiere;
	}

	public void setNomMatiere(String nomMatiere) {
		this.nomMatiere = nomMatiere;
	}

	public Long getEnsKey() {
		return ensKey;
	}

	public void setEnsKey(Long ensKey) {
		this.ensKey = ensKey;
	}

	public CoursId(String nomMatiere, Long ensKey) {
		super();
		this.nomMatiere = nomMatiere;
		this.ensKey = ensKey;
	}

	public CoursId() {
	}
}
