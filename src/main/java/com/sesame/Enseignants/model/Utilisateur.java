package com.sesame.Enseignants.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
@Inheritance(strategy=InheritanceType.JOINED)  
public class Utilisateur {

	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	//@Column(name = "nom", length = 30, nullable = false)
	private String nom;
	//@Column(name = "type", length = 30, nullable = false)
	private String type;
	//@Column(name = "prenom", length = 30, nullable = false)
	private String prenom;
	
	private String email ;
	private String password ; 
	private String role ;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Utilisateur(Long id, String nom, String type, String prenom, String email, String password, String role) {
		super();
		this.id = id;
		this.nom = nom;
		this.type = type;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.role = role;
	}
	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", type=" + type + ", prenom=" + prenom + ", email=" + email
				+ ", password=" + password + ", role=" + role + "]";
	} 
	
	
	
	
	
}
